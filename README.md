Repository containing mongo queries for ArrayMap mongo data samples (200).

Queries are contained inside folder [mongo-queries](mongo-queries)

One thing that could be done is to transform the mongo data into VCF format and use the Python Simple Beacon Server
https://github.com/maximilianh/ucscBeacon