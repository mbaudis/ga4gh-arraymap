/**
 * Created by sduvaud on 23/02/16.
 */
var matches = [];

var groupByQuery = db.samples.aggregate(
    [
        { $group : { _id : "$ICDMORPHOLOGYCODE", description: { $first: "$ICDMORPHOLOGY"}, uid: { $push: "$UID" } } }
    ]
);

groupByQuery.forEach(function (match) {
    var samples = match.uid;
    var description = match.description;
    var genome = 'reference genome';

    matches.push('\n{"id": "' + match._id + '", "description": "' + description+ '", "reference": "' + genome + '", "size": { "variants": "-1", "samples": "' + samples.length + '"}}');
});

print ('{ "info": {\n "id": "arraymap-beacon",\n "name": "Beacon ArrayMap", \n "organization": "SIB Swiss Institute of Bioinformatics",\n "description": "First Prototype of a Beacon v0.2 implementation for ArrayMap.",\n "datasets": [');
print(matches);
print ('],\n "api": "v0.2",\n "homepage": "http://beacon-arraymap.vital-it.ch/",\n "email": "SIB-Technology@isb-sib.ch"\n }\n}');