/*db.samples2.find().forEach(function(sample) {
    if(sample.SEGMENTS_HG18){
        sample.SEGMENTS_HG18.forEach(function(segment) {
            segment.SEGSTART = parseInt(segment.SEGSTART, 10);
            segment.SEGSTOP = parseInt(segment.SEGSTOP, 10);
        });
        //prettyPrint(sample);
        db.samples2.save(sample);
    }
});*/

/*prettyPrint(db.samples2.count({$and: ['ICDMORPHOLOGYCODE': '8010/3', 'SEGMENTS_HG18': { '$elemMatch': { 'CHRO': '9'}}, 'SEGMENTS_HG18': { '$elemMatch': { 'SEGSTOP': {$gte :57649422}}}, 'SEGMENTS_HG18': { '$elemMatch': { 'SEGSTART': {$lte: 57649422 }}}]}));*/
//check https://genome.ucsc.edu/FAQ/FAQreleases.html
var references = ['SEGMENTS_HG38','SEGMENTS_HG19','SEGMENTS_HG18'];

db.samples.find().forEach(function(sample) {
    _.forEach(references, function(ref){
        if(sample[ref]){
            sample[ref].forEach(function(segment) {
                segment.SEGSTART = parseInt(segment.SEGSTART, 10);
                segment.SEGSTOP = parseInt(segment.SEGSTOP, 10);
            });
            //prettyPrint(sample);
            db.samples.save(sample);
        }
    })
});
