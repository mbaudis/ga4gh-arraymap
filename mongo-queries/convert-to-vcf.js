db.samples.find().limit(1).forEach(function(sample) {
    sample.SEGMENTS_HG18.forEach(function(segment) {
        //Format of VCF, example: https://github.com/maximilianh/ucscBeacon/blob/master/test/icgcTest.vcf
        // #CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO
        print(segment.CHRO + "\t" + segment.SEGSTART +  "\t.\t.\t.\t.\t.\t");
    });
});
