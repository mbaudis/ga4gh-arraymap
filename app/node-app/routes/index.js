var express = require('express');
var router = express.Router();
var _ = require('underscore')
var beacon = require('../arraymap-beacon/v0.4/arraymap-beacon.js');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.sendfile('index.html');
    //res.render('index', { title: 'Express' });
});

/* GET home page. */
router.get('/documentation', function (req, res, next) {
    res.sendfile('public/index.html');
    //res.render('index', { title: 'Express' });
});

/* GET documentation page.
router.get('/documentation', function (req, res) {
    res.render('doc', {
        title: 'Beacon arrayMap - Documentation'
    });
});*/

/* GET home page. */
router.get('/info', function (req, res, next) {
    res.json(beacon.info);
});

/* Shows samples */
router.get('/samples/', function (req, res) {
    var db = req.db;
    if (req.query.limit) {
        db.samples.find({}).limit(parseInt(req.query.limit)).skip(1, function (e, docs) {
            res.json(docs);
        });
    } else {
        db.samples.find({}, {}, function (e, docs) {
            res.json(docs);
        });
    }
});

/* Returns response for API v 0.2  */
router.get('/v0.2/query/', function (req, res) {
    var beacon = require('../arraymap-beacon/v0.2/arraymap-beacon.js');
    var preconditions = beacon.checkPreconditions(req.query);

    if (preconditions.hasError) {
        res.json(preconditions.msg); //Gets error messages
        return;
    }
    var mongoQuery = beacon.buildMongoQuery(req.query);
    console.log("Building MongoDB query params: " + JSON.stringify(mongoQuery));

    req.db.samples.count({'ICDMORPHOLOGYCODE': req.query.dataset}, function(err, count){
            req.db.samples.find(mongoQuery, {}, function (err, docs){
            var response = beacon.checkResultAndGetResponse(req.query, docs, count);
            response.NOT_BEACON_totalInDataSet = count;
            res.json(response);
        });
    }
    )

});

/* Returns response for API v 0.3  */
router.get('/v0.3/query/', function (req, res) {

    var preconditions = beacon.checkPreconditions(req.query);

    if (preconditions.hasError) {
        res.json(preconditions.msg); //Gets error messages
        return;
    }

    var mongoQuery = beacon.buildMongoQuery(req.query);
    console.log("Building MongoDB query params: " + JSON.stringify(mongoQuery));

    req.db.samples.aggregate(mongoQuery, function(err, docs) {
        var response = beacon.checkResultAndGetResponse(req.query, docs);
        res.json(response);
    });
});

router.get('/v0.4/dataset', function (req, res) {

    var preconditions = beacon.checkDatasetIdentifier(req.query);

    if (preconditions.hasError) {
        res.json(preconditions.msg);
        return;
    }
    var mongoQuery = beacon.buildMongoDatasetQuery(req.query);
    console.log("Building MongoDB query params: " + JSON.stringify(mongoQuery));

    req.db.samples.aggregate(mongoQuery, function(err, docs){
        if (docs && docs.length > 0) {
            var response = beacon.checkDatasetResultAndGetResponse(req.query, docs);
            res.json(response);
        }
        else {
            res.json("No dataset found");
        }
    });
});

/* Returns response for API v 0.4  */
router.get('/v0.4/query/', function (req, res) {

    var preconditions = beacon.checkPreconditions(req.query);

    if (preconditions.hasError) {
        res.json(preconditions.msg); //Gets error messages
        return;
    }

    var mongoQuery = beacon.buildMongoQuery(req.query);
    console.log("Building MongoDB query params: " + JSON.stringify(mongoQuery));

    req.db.samples.aggregate(mongoQuery, function(err, docs) {
        var response = beacon.checkResultAndGetResponse(req.query, docs);
        res.json(response);
    });
});

module.exports = router;