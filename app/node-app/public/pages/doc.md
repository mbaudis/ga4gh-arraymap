## Current implementation

This beacon queries 20K samples coming from ArrayMap from here:
<a href="http://arraymap.org/api/?db=arraymap&qcveto=1&api_out=samples&icdm_m=8,9&api_doctype=jsondata">http://arraymap.org/api/?db=arraymap&qcveto=1&api_out=samples&icdm_m=8,9&api_doctype=jsondata</a>

<img src="/img/current_implementation.jpg" class="img-thumbnail img-responsive">

**Comments**

* reference genome: In [In Beacon 0.2 specifications](https://docs.google.com/document/d/154GBOixuZxpoPykGKcPOyrYUcgEXVe2NvKx61P4Ybn4/edit), the values expected for human data is of GRCh format, whereas arrayMap uses HG nomenclature. A mapping 1:1 is done on the fly to convert from GRCh to HG.
* allele: [In Beacon 0.2 specifications](https://docs.google.com/document/d/154GBOixuZxpoPykGKcPOyrYUcgEXVe2NvKx61P4Ybn4/edit), the parameter is required. However, it does not make sense at all for arrayMap (in which no sequence related data exists).
* variantClass: This is a new parameter. The value equals DUP corresponds to SEGTYPE=1 (gain) in arrayMap, while DEL is for SEGTYPE=-1 (loss). The parameter is mandatory.
* sampleuid: This is DEBUG info.
* matchedSegment: This is DEBUG info.
* NOT_BEACON_totalInDataSet: This is also debug info but could be nice to include it as well on Beacon

**Example**

Query chromosome 11 at position 34439881 for the dataset 8070/3, showing a deletion: [query?chromosome=11&position=34439881&reference=GRCh38&dataset=8070/3&variantClass=DEL](http://beacon-arraymap.vital-it.ch/v0.2/query?chromosome=11&position=34439881&dataset=8070/3&variantClass=DEL)


**Outcome from the meeting with Jordi (Feb, 10, 2016):**

It should be ok to have the variantClass in Beacon v0.3. As for ranges, this is more complicated and requires lots of discussions. We will probably go in 2 steps:

* add P (duPlication) to the list of allowed values for the allele parameter.
* modify I (insertion) slightly by making the "sequence" string optional, i.e. be fuzzy about what was inserted.

and then

* proper variantClass
* ranges (defined as start/stop position, e.g. [200000,30000] **or** start and length, e.g. 20000:100000)


## API description


**QueryResource**

<table class="table table-responsive table-striped">
        <tbody>
            <tr>
                <th style="width: 25%">Parameter name</th>
                <th style="width: 25%">Required in Beacon</th>
                <th style="width: 25%">Required in Beacon-arrayMap</th>
                <th style="width: 25%">Comment</th>
            </tr>
            <tr>
                <td>referenceName</td>
                <td>yes</td>
                <td>yes</td>
                <td>no control for now (dropdown list?)</td>
            </tr>
            <tr class="text-danger">
                <td>start</td>
                <td>yes</td>
                <td>yes</td>
                <td>no control for now (&gt;0)</td>
            </tr>
            <tr>
                <td>assemblyId</td>
                <td>yes</td>
                <td>yes</td>
                <td>Genome ID. Should be added</td>
            </tr>
            <tr class="text-danger">
                <td>alternateBases</td>
                <td>yes</td>
                <td>yes</td>
                <td>DEL or DUP</td>
            </tr>
            <tr>
                <td>datasetIds</td>
                <td>no</td>
                <td>no</td>
                <td>ICDM numbers list. Default: all</td>
            </tr>
        </tbody>
    </table>


**ResponseResource**
<table class="table table-responsive table-striped">
        <tbody>
            <tr>
                <th style="width: 25%">Parameter name</th>
                <th style="width: 25%">Required in Beacon</th>
                <th style="width: 25%">Required in Beacon-arrayMap</th>
                <th style="width: 25%">Comment</th>
            </tr>
            <tr>
                <td>exists</td>
                <td>yes</td>
                <td>yes</td>
                <td>Boolean</td>
            </tr>
            <tr>
                <td>error</td>
                <td>no</td>
                <td>no</td>
                <td></td>
            </tr>
            <tr>
                <td>note</td>
                <td>no</td>
                <td>no</td>
                <td></td>
            </tr>
            <tr>
                <td>beaconId</td>
                <td>yes</td>
                <td>yes</td>
                <td>value=ok</td>
            </tr>
            <tr>
                <td>BeaconRequest</td>
                <td>no</td>
                <td>no</td>
                <td></td>
            </tr>
        </tbody>
    </table>

## Forthcoming implementation: Open questions


**Imprecise structural variants:**

Exact positions are definitely not suitable for arrayMap. We should be able to request for ranges rather than positions. However, the issue with using ranges is that the start and end positions are imprecise in arrayMap.

<div class="alert alert-info">
In the VCF, there is a field for the symbolic alternate alleles for imprecise structural variants in the meta-information lines.
<br>See <a target="_blank" href="https://samtools.github.io/hts-specs/VCFv4.2.pdf">the VCF 4.2 specifications</a> for more details.
</div>

**Example:**


 <table class="table">
        <tbody>
            <tr>
                <th>CHROM</th>
                <th>POS</th>
                <th>ID</th>
                <th>REF</th>
                <th>ALT</th>
                <th>QUAL</th>
                <th>FILTER</th>
                <th>INFO</th>
                <th>FORMAT</th>
                <th>NA00001</th>
            </tr>
            <tr>
                <td>1</td>
                <td>2827694</td>
                <td>rs2376870</td>
                <td>CGTGGATGCGGGGAC</td>
                <td>C</td>
                <td>.</td>
                <td>PASS</td>
                <td>SVTYPE=DEL;END=2827762;HOMLEN=1;HOMSEQ=G;SVLEN=-68</td>
                <td>GT:GQ</td>
                <td>1/1:13.9</td>
            </tr>
            <tr>
                <td>1</td>
                <td>12665100</td>
                <td>.</td>
                <td>A</td>
                <td></td>
                <td>14</td>
                <td>PASS</td>
                <td>
                SVTYPE=DUP;END=12686200;SVLEN=21100;CIPOS=-500,500;CIEND=-500,500</td>
                <td>GT:GQ:CN:CNQ</td>
                <td>./.:0:3:16.2</td>
            </tr>
        </tbody>
    </table>

Description of the INFO field:

* SVTYPE=(DEL|DUP|INS|CNV) - mandatory: Type of structural variant.
* END=integer - mandatory: End position of the variant
* SVLEN=integer - Diff in length btw Ref and Alt. Negative in case of deletion.
* CIPOS=integer - Confidence interval around position (POS).
* CIEND=integer - Confidence interval around end position (END).

**Use-cases:**

Now, how should we proceed with the ranges in the following cases?
<div class="row">
<div class="col-md-3">
    <img class="img-thumbnail img-responsive" src=
    "/img/chromosome15.png"><em>Only either start or stop postion
    are in the CNV region, i.e. the queries range is not fully
    covered.</em>
</div>
<div class="col-md-3">
    <img class="img-thumbnail img-responsive" src=
    "/img/chromosome20.png"><em>The range is embedded within a CNV
    region.</em>
</div>
<div class="col-md-3">
    <img class="img-thumbnail img-responsive" src=
    "/img/chromosome13.png"><em>The range matches 2 CNV
    regions.</em>
</div>
<div class="col-md-3">
    <img class="img-thumbnail img-responsive" src="/img/chromosome11.png"><em>The 2 range overlaps many CNV
    regions.</em>
</div>

##Info endpoint


Open questions:

<img src="/img/info_structure.png" class="img-thumbnail img-responsive">

*Truncated version*

* In [Beacon 0.2 specifications](https://docs.google.com/document/d/154GBOixuZxpoPykGKcPOyrYUcgEXVe2NvKx61P4Ybn4/edit), 'variants' is a mandatory property of DataSizeResource and its value was arbitrary set to -1. Now, we have to decide whether we should:
    1. stick to -1.
    2. use the property 'variants' to count the number of segments (for instance). - 9 March 2016 - APPROVED.
    3. ask the Beacon team if we can make this optional property.
    4. ask the Beacon team if we can change the property name.
    

--

* In [Beacon 0.2 specifications](https://docs.google.com/document/d/154GBOixuZxpoPykGKcPOyrYUcgEXVe2NvKx61P4Ybn4/edit), 'reference' is a mandatory property of DataSetResource. The problem is that, in arrayMap, the genome version is linked to a given sample, and not to a cancer tissue type. Moreover, there can be more than one genome version in a sample. Now, we have to decide whether we should:
   1. assign it a fake default value.
   2. assign it the first reference genome's value of the data structure.
   3. ask the Beacon team if they can make this optional property.
   4. ask the Beacon team if they can replace the property type by a list of strings (instead of a string).
   5. clarify CIPOS and CIEND in VCF v4.2 for DUP/DEL (i.e. CIPOS=-500,500;CIEND=-500,500)- OK
     * It's basically a range. See example and our interpretation here: https://github.com/samtools/hts-specs/issues/132
     * For arrayMap, we need to define that interval for POS and END position. Michael suggested something like 0.5 * average probe distance (would be in order of kB)