var arrayMap = angular.module('beaconArrayApp', ['ngRoute', 'ng-showdown']);
arrayMap.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {

        $locationProvider.html5Mode(true);

        $routeProvider.
        when('/', {
            templateUrl: 'partials/beacon-home.html'
        }).
        when('/documentation', {
            templateUrl: 'partials/beacon-doc.html'
        });
    }]);

arrayMap.controller('BeaconController', ['$scope', '$location', function ($scope, $location) {

    $scope.references = ['GRCh38','GRCh37','GRCh36'];

    $scope.config = {
        "referenceName": "2",
        "start": 42049214,
        "assemblyId": "GRCh38",
        "datasetIds": "8010/2",
        "alternateBases": "DEL",
        "length": '85689'
    };

    $scope.datasetConfig = {
        "id": "all"
    };

    $scope.getNewApiUrl = function () {

        var conf = $scope.config;

        return $location.absUrl() + "v0.4/query?" +
            "referenceName=" + conf.referenceName +
            "&start=" + conf.start +
            "&assemblyId=" + conf.assemblyId +
            "&datasetIds=" + conf.datasetIds +
            ((conf.alternateBases === "") ? "all" : "&alternateBases=" + conf.alternateBases) +
            ((conf.length === '') ? "" : conf.length);
    };

    $scope.getInfoUrl = function () {
        return $location.absUrl() + "info"
    };

    $scope.getDatasetUrl = function () {

        var conf = $scope.datasetConfig;
        return $location.absUrl() + "v0.4/dataset?" +
                "id=" + conf.id;
    };

    $scope.getApiUrl = function () {

        var conf = $scope.config;

        return $location.absUrl() + "v0.2/query?chromosome=" +
            conf.chromosome +
            "&position=" + conf.position +
            "&reference=" + conf.reference +
            "&dataset=" + conf.dataset +
            ((conf.variantClass === "") ? "" : "&variantClass=" + conf.variantClass);
    };


    $scope.datasets = [
        {
            "code": "all",
            "description": "All datasets"
        },
        {
            "code": "0000/0",
            "description": "not classified in icd-o 3 [e.g. non-neoplastic or benign]"
        },
        {
            "code": "8000/3",
            "description": "neoplasm, malignant"
        },
        {
            "code": "8010/0",
            "description": "epithelial tumor, benign"
        },
        {
            "code": "8010/2",
            "description": "carcinoma in situ, nos"
        },
        {
            "code": "8010/3",
            "description": "carcinoma, nos"
        },
        {
            "code": "8012/3",
            "description": "large cell carcinoma, nos"
        },
        {
            "code": "8013/3",
            "description": "large cell neuroendocrine carcinoma"
        },
        {
            "code": "8020/3",
            "description": "carcinoma, undifferentiated type, nos"
        },
        {
            "code": "8021/3",
            "description": "carcinoma, anaplastic type, nos"
        },
        {
            "code": "8022/3",
            "description": "pleomorphic carcinoma"
        },
        {
            "code": "8031/3",
            "description": "giant cell carcinoma"
        },
        {
            "code": "8032/3",
            "description": "spindle cell carcinoma"
        },
        {
            "code": "8033/3",
            "description": "sarcomatoid carcinoma"
        },
        {
            "code": "8041/3",
            "description": "small cell carcinoma, nos"
        },
        {
            "code": "8046/3",
            "description": "non-small cell carcinoma"
        },
        {
            "code": "8050/3",
            "description": "papillary carcinoma, nos"
        },
        {
            "code": "8052/0",
            "description": "squamous cell papilloma"
        },
        {
            "code": "8070/1",
            "description": "premalignant squamous epithelium, nos"
        },
        {
            "code": "8070/2",
            "description": "squamous cell carcinoma in situ, nos"
        },
        {
            "code": "8070/3",
            "description": "squamous cell carcinoma, nos"
        },
        {
            "code": "8071/0",
            "description": "squamous cell keratosis, nos"
        },
        {
            "code": "8075/3",
            "description": "squamous cell carcinoma, acantholytic"
        },
        {
            "code": "8077/2",
            "description": "squamous intraepithelial neoplasia, grade iii"
        },
        {
            "code": "8082/3",
            "description": "undifferentiated nasopharyngeal carcinoma"
        },
        {
            "code": "8090/3",
            "description": "basal cell carcinoma, nos"
        },
        {
            "code": "8120/2",
            "description": "transitional cell carcinoma in situ"
        },
        {
            "code": "8120/3",
            "description": "transitional cell carcinoma, nos"
        },
        {
            "code": "8130/1",
            "description": "urothelial papilloma, nos"
        },
        {
            "code": "8130/2",
            "description": "papillary transitional cell carcinoma, non-invasive"
        },
        {
            "code": "8130/3",
            "description": "papillary transitional cell carcinoma"
        },
        {
            "code": "8140/0",
            "description": "adenoma, nos"
        },
        {
            "code": "8140/1",
            "description": "atypical adenoma"
        },
        {
            "code": "8140/2",
            "description": "adenocarcinoma in situ"
        },
        {
            "code": "8140/3",
            "description": "adenocarcinoma, nos"
        },
        {
            "code": "8144/3",
            "description": "adenocarcinoma, intestinal type"
        },
        {
            "code": "8145/3",
            "description": "carcinoma, diffuse type"
        },
        {
            "code": "8148/2",
            "description": "glandular intraepithelial neoplasia, grade iii"
        },
        {
            "code": "8150/1",
            "description": "islet cell adenoma"
        },
        {
            "code": "8150/3",
            "description": "islet cell carcinoma"
        },
        {
            "code": "8151/1",
            "description": "insulinoma, nos"
        },
        {
            "code": "8151/3",
            "description": "insulinoma, malignant"
        },
        {
            "code": "8152/1",
            "description": "glucagonoma, nos"
        },
        {
            "code": "8152/3",
            "description": "glucagonoma, malignant"
        },
        {
            "code": "8153/1",
            "description": "gastrinoma, nos"
        },
        {
            "code": "8153/3",
            "description": "gastrinoma, malignant"
        },
        {
            "code": "8155/3",
            "description": "vipoma"
        },
        {
            "code": "8156/1",
            "description": "somatostatinoma, nos"
        },
        {
            "code": "8160/3",
            "description": "cholangiocarcinoma"
        },
        {
            "code": "8161/3",
            "description": "bile duct cystadenocarcinoma"
        },
        {
            "code": "8170/0",
            "description": "hepatocellular adenoma, nos"
        },
        {
            "code": "8170/3",
            "description": "hepatocellular carcinoma, nos"
        },
        {
            "code": "8171/3",
            "description": "hepatocellular carcinoma, fibrolamellar"
        },
        {
            "code": "8180/3",
            "description": "combined hepatocellular carcinoma and cholangiocarcinoma"
        },
        {
            "code": "8200/3",
            "description": "adenoid cystic carcinoma"
        },
        {
            "code": "8201/3",
            "description": "ductal carcinoma, cribriform type [c50._]"
        },
        {
            "code": "8210/3",
            "description": "adenocarcinoma in adenomatous polyp"
        },
        {
            "code": "8211/0",
            "description": "tubular adenoma, nos"
        },
        {
            "code": "8211/3",
            "description": "tubular adenocarcinoma"
        },
        {
            "code": "8215/3",
            "description": "adenocarcinoma of anal ducts [c21.1]"
        },
        {
            "code": "8220/3",
            "description": "adenocarcinoma in adenomatous polyposis coli"
        },
        {
            "code": "8240/3",
            "description": "carcinoid tumor, nos"
        },
        {
            "code": "8246/3",
            "description": "neuroendocrine carcinoma"
        },
        {
            "code": "8247/3",
            "description": "merkel cell carcinoma"
        },
        {
            "code": "8249/3",
            "description": "atypical carcinoid tumor"
        },
        {
            "code": "8250/3",
            "description": "bronchiolo-alveolar adenocarcinoma"
        },
        {
            "code": "8251/3",
            "description": "alveolar adenocarcinoma"
        },
        {
            "code": "8252/3",
            "description": "bronchiolo-alveolar carcinoma, non-mucinous"
        },
        {
            "code": "8254/3",
            "description": "bronchiolo-alveolar carcinoma, indeterminate type [c34._]"
        },
        {
            "code": "8260/0",
            "description": "papillary adenoma, nos"
        },
        {
            "code": "8260/3",
            "description": "papillary adenocarcinoma, nos"
        },
        {
            "code": "8263/0",
            "description": "tubullovillous adenoma, nos"
        },
        {
            "code": "8271/0",
            "description": "prolactinoma"
        },
        {
            "code": "8272/0",
            "description": "pituitary adenoma, typical"
        },
        {
            "code": "8272/3",
            "description": "pituitary carcinoma, nos"
        },
        {
            "code": "8290/0",
            "description": "oncocytoma"
        },
        {
            "code": "8290/3",
            "description": "follicular carcinoma, oxyphilic cell"
        },
        {
            "code": "8310/3",
            "description": "clear cell renal cell carcinoma"
        },
        {
            "code": "8312/3",
            "description": "renal cell carcinoma"
        },
        {
            "code": "8317/3",
            "description": "renal cell carcinoma, chromophobe"
        },
        {
            "code": "8318/3",
            "description": "renal cell carcinoma, spindle cell [c64.9]"
        },
        {
            "code": "8319/3",
            "description": "renal medullary carcinoma"
        },
        {
            "code": "8330/0",
            "description": "follicular adenoma, nos"
        },
        {
            "code": "8333/0",
            "description": "fetal adenoma"
        },
        {
            "code": "8333/3",
            "description": "fetal adenocarcinoma"
        },
        {
            "code": "8335/3",
            "description": "follicular carcinoma"
        },
        {
            "code": "8340/3",
            "description": "papillary carcinoma, follicular variant"
        },
        {
            "code": "8361/0",
            "description": "juxtaglomerular tumor [c64.9]"
        },
        {
            "code": "8370/0",
            "description": "adrenal cortical adenoma"
        },
        {
            "code": "8370/3",
            "description": "adrenal cortical carcinoma"
        },
        {
            "code": "8380/1",
            "description": "endometrioid dysplasia"
        },
        {
            "code": "8380/3",
            "description": "endometrioid carcinoma, nos"
        },
        {
            "code": "8382/3",
            "description": "endometrioid adenocarcinoma, secretory variant"
        },
        {
            "code": "8384/3",
            "description": "adenocarcinoma, endocervical type"
        },
        {
            "code": "8401/2",
            "description": "apocrine dcis"
        },
        {
            "code": "8401/3",
            "description": "apocrine adenocarcinoma"
        },
        {
            "code": "8430/3",
            "description": "mucoepidermoid carcinoma"
        },
        {
            "code": "8440/3",
            "description": "cystadenocarcinoma, nos"
        },
        {
            "code": "8441/3",
            "description": "serous adenocarcinoma, nos"
        },
        {
            "code": "8442/1",
            "description": "serous cystadenoma, borderline malignancy"
        },
        {
            "code": "8450/3",
            "description": "papillary cystadenocarcinoma, nos"
        },
        {
            "code": "8453/0",
            "description": "intraductal papillary-mucinous adenoma [c25._]"
        },
        {
            "code": "8453/1",
            "description": "intraductal papillary-mucinous tumor with moderate dysplasia [c25._]"
        },
        {
            "code": "8453/2",
            "description": "intraductal papillary-mucinous carcinoma, non-invasive [c25._]"
        },
        {
            "code": "8453/3",
            "description": "intraductal papillary-mucinous carcinoma, invasive [c25._]"
        },
        {
            "code": "8460/3",
            "description": "micropapillary serous carcinoma [c56.9]"
        },
        {
            "code": "8461/3",
            "description": "serous surface papillary carcinoma"
        },
        {
            "code": "8462/1",
            "description": "serous papillary cystic tumor of borderline malignancy"
        },
        {
            "code": "8470/3",
            "description": "mucinous cystadenocarcinoma, nos"
        },
        {
            "code": "8472/1",
            "description": "mucinous cystic tumor of borderline malignancy"
        },
        {
            "code": "8480/3",
            "description": "mucinous adenocarcinoma"
        },
        {
            "code": "8481/3",
            "description": "mucin-producing adenocarcinoma"
        },
        {
            "code": "8490/3",
            "description": "signet ring cell carcinoma"
        },
        {
            "code": "8500/2",
            "description": "ductal carcinoma in situ, nos"
        },
        {
            "code": "8500/3",
            "description": "invasive carcinoma of no special type"
        },
        {
            "code": "8501/2",
            "description": "dcis, comedo type [c50._]"
        },
        {
            "code": "8501/3",
            "description": "comedocarcinoma, nos"
        },
        {
            "code": "8502/3",
            "description": "secretory carcinoma of breast"
        },
        {
            "code": "8503/2",
            "description": "ductal carcinoma in situ, papillary [c50._]"
        },
        {
            "code": "8503/3",
            "description": "intraductal papillary adenocarcinoma with invasion"
        },
        {
            "code": "8507/2",
            "description": "intraductal micropapillary carcinoma"
        },
        {
            "code": "8507/3",
            "description": "invasive micropapillary carcinoma"
        },
        {
            "code": "8510/3",
            "description": "medullary carcinoma, nos"
        },
        {
            "code": "8513/3",
            "description": "atypical medullary carcinoma"
        },
        {
            "code": "8520/2",
            "description": "lobular carcinoma in situ"
        },
        {
            "code": "8520/3",
            "description": "invasive lobular carcinoma"
        },
        {
            "code": "8522/3",
            "description": "infiltrating duct and lobular carcinoma"
        },
        {
            "code": "8523/3",
            "description": "infiltrating duct mixed with other types of carcinoma [c50._]"
        },
        {
            "code": "8524/3",
            "description": "infiltrating lobular mixed with other types of carcinoma"
        },
        {
            "code": "8530/3",
            "description": "inflammatory carcinoma"
        },
        {
            "code": "8541/3",
            "description": "paget disease and infiltrating duct carcinoma"
        },
        {
            "code": "8550/3",
            "description": "acinar cell carcinoma"
        },
        {
            "code": "8560/3",
            "description": "adenosquamous carcinoma"
        },
        {
            "code": "8562/3",
            "description": "myoepithelial carcinoma"
        },
        {
            "code": "8570/3",
            "description": "adenocarcinoma with squamous metaplasia"
        },
        {
            "code": "8575/3",
            "description": "metaplastic carcinoma, nos"
        },
        {
            "code": "8576/3",
            "description": "hepatoid adenocarcinoma"
        },
        {
            "code": "8581/3",
            "description": "thymoma, medullary [who a], malignant"
        },
        {
            "code": "8585/3",
            "description": "thymoma, atypical [who b3], malignant"
        },
        {
            "code": "8586/3",
            "description": "thymoma, type c [c37.9]"
        },
        {
            "code": "8589/3",
            "description": "carcinoma showing thymus-like differentiation"
        },
        {
            "code": "8620/3",
            "description": "granulosa cell tumor, malignant"
        },
        {
            "code": "8640/3",
            "description": "sertoli cell carcinoma"
        },
        {
            "code": "8680/1",
            "description": "paraganglioma, nos"
        },
        {
            "code": "8682/1",
            "description": "parasympathetic paraganglioma"
        },
        {
            "code": "8693/3",
            "description": "extra-adrenal paraganglioma, malignant"
        },
        {
            "code": "8700/0",
            "description": "pheochromocytoma, nos"
        },
        {
            "code": "8700/3",
            "description": "pheochromocytoma, malignant"
        },
        {
            "code": "8720/0",
            "description": "pigmented nevus, nos"
        },
        {
            "code": "8720/3",
            "description": "malignant melanoma, nos"
        },
        {
            "code": "8721/3",
            "description": "nodular melanoma"
        },
        {
            "code": "8730/3",
            "description": "amelanotic melanoma"
        },
        {
            "code": "8742/3",
            "description": "lentigo maligna melanoma"
        },
        {
            "code": "8743/3",
            "description": "superficial spreading melanoma"
        },
        {
            "code": "8744/3",
            "description": "acral lentiginous melanoma, malignant"
        },
        {
            "code": "8761/0",
            "description": "congenital melanocytic nevus"
        },
        {
            "code": "8770/0",
            "description": "spitz nevus"
        },
        {
            "code": "8770/3",
            "description": "mixed epithelial and spindle cell melanoma"
        },
        {
            "code": "8780/3",
            "description": "blue nevus, malignant"
        },
        {
            "code": "8800/3",
            "description": "sarcoma, nos"
        },
        {
            "code": "8802/3",
            "description": "giant cell sarcoma"
        },
        {
            "code": "8804/3",
            "description": "epithelioid sarcoma"
        },
        {
            "code": "8805/3",
            "description": "undifferentiated sarcoma"
        },
        {
            "code": "8810/0",
            "description": "fibroma, nos"
        },
        {
            "code": "8810/3",
            "description": "fibrosarcoma, nos"
        },
        {
            "code": "8811/0",
            "description": "fibromyxoid tumor, nos"
        },
        {
            "code": "8811/3",
            "description": "myxoinflammatory fibroblastic sarcoma"
        },
        {
            "code": "8815/0",
            "description": "solitary fibrous tumor"
        },
        {
            "code": "8815/3",
            "description": "solitary fibrous tumor, malignant"
        },
        {
            "code": "8820/0",
            "description": "elastofibroma"
        },
        {
            "code": "8821/1",
            "description": "desmoid-type fibromatosis"
        },
        {
            "code": "8823/0",
            "description": "desmoplastic fibroma"
        },
        {
            "code": "8825/0",
            "description": "myofibroblastic tumor, nos"
        },
        {
            "code": "8825/1",
            "description": "inflammatory myofibroblastic tumour"
        },
        {
            "code": "8830/0",
            "description": "fibrous histiocytoma, nos"
        },
        {
            "code": "8830/3",
            "description": "fibrous histiocytoma, malignant"
        },
        {
            "code": "8832/3",
            "description": "dermatofibrosarcoma, nos"
        },
        {
            "code": "8833/3",
            "description": "pigmented dermatofibrosarcoma protuberans"
        },
        {
            "code": "8850/0",
            "description": "lipoma, nos"
        },
        {
            "code": "8850/3",
            "description": "liposarcoma, nos"
        },
        {
            "code": "8851/3",
            "description": "lipoma-like liposarcoma"
        },
        {
            "code": "8852/3",
            "description": "liposarcoma, myxoid"
        },
        {
            "code": "8854/3",
            "description": "liposarcoma, pleomorphic"
        },
        {
            "code": "8855/3",
            "description": "mixed type liposarcoma"
        },
        {
            "code": "8858/3",
            "description": "liposarcoma, dedifferentiated"
        },
        {
            "code": "8890/0",
            "description": "leiomyoma, nos"
        },
        {
            "code": "8890/3",
            "description": "leiomyosarcoma, nos"
        },
        {
            "code": "8891/3",
            "description": "epithelioid leiomyosarcoma"
        },
        {
            "code": "8894/0",
            "description": "angioleiomyoma"
        },
        {
            "code": "8895/3",
            "description": "myosarcoma"
        },
        {
            "code": "8900/3",
            "description": "rhabdomyosarcoma, nos"
        },
        {
            "code": "8901/3",
            "description": "rhabdomyosarcoma, pleomorphic"
        },
        {
            "code": "8910/3",
            "description": "embryonal rhabdomyosarcoma"
        },
        {
            "code": "8920/3",
            "description": "alveolar rhabdomyosarcoma"
        },
        {
            "code": "8930/3",
            "description": "endometrial stromal sarcoma"
        },
        {
            "code": "8931/3",
            "description": "endometrial stromal sarcoma, low grade"
        },
        {
            "code": "8936/0",
            "description": "gastrointestinal stromal tumor, benign"
        },
        {
            "code": "8936/1",
            "description": "gastrointestinal stromal tumor, uncertain malignant potential"
        },
        {
            "code": "8936/3",
            "description": "gastrointestinal stromal tumor, malignant"
        },
        {
            "code": "8950/3",
            "description": "mullerian mixed tumor"
        },
        {
            "code": "8951/3",
            "description": "mesodermal mixed tumor"
        },
        {
            "code": "8960/3",
            "description": "nephroblastoma, nos"
        },
        {
            "code": "8963/3",
            "description": "rhabdoid tumor, nos"
        },
        {
            "code": "8964/3",
            "description": "clear cell sarcoma of kidney"
        },
        {
            "code": "8970/3",
            "description": "hepatoblastoma"
        },
        {
            "code": "8973/3",
            "description": "pleuropulmonary blastoma"
        },
        {
            "code": "8980/3",
            "description": "carcinosarcoma, nos"
        },
        {
            "code": "8982/0",
            "description": "myoepithelioma"
        },
        {
            "code": "8982/3",
            "description": "malignant myoepithelioma"
        },
        {
            "code": "8990/3",
            "description": "mesenchymoma, malignant"
        },
        {
            "code": "8991/3",
            "description": "embryonal sarcoma"
        },
        {
            "code": "9010/0",
            "description": "fibroadenoma, nos"
        },
        {
            "code": "9020/0",
            "description": "phyllodes tumor, benign"
        },
        {
            "code": "9020/1",
            "description": "phyllodes tumor, borderline"
        },
        {
            "code": "9020/3",
            "description": "phyllodes tumor, malignant"
        },
        {
            "code": "9040/3",
            "description": "synovial sarcoma, nos"
        },
        {
            "code": "9041/3",
            "description": "synovial sarcoma, monophasic"
        },
        {
            "code": "9043/3",
            "description": "synovial sarcoma, biphasic"
        },
        {
            "code": "9050/3",
            "description": "mesothelioma, nos"
        },
        {
            "code": "9051/3",
            "description": "desmoplastic mesothelioma"
        },
        {
            "code": "9052/3",
            "description": "epithelioid mesothelioma, malignant"
        },
        {
            "code": "9053/3",
            "description": "mesothelioma, biphasic, malignant"
        },
        {
            "code": "9060/3",
            "description": "dysgerminoma"
        },
        {
            "code": "9061/3",
            "description": "seminoma, nos"
        },
        {
            "code": "9063/3",
            "description": "spermatocytic seminoma"
        },
        {
            "code": "9064/2",
            "description": "intratubular germ cell neoplasia [c62._]"
        },
        {
            "code": "9064/3",
            "description": "germinoma"
        },
        {
            "code": "9065/3",
            "description": "germ cell tumor, nonseminomatous"
        },
        {
            "code": "9070/3",
            "description": "embryonal carcinoma, nos"
        },
        {
            "code": "9071/3",
            "description": "yolk sac tumor"
        },
        {
            "code": "9080/0",
            "description": "teratoma, benign"
        },
        {
            "code": "9080/3",
            "description": "teratoma, malignant, nos"
        },
        {
            "code": "9081/3",
            "description": "teratocarcinoma"
        },
        {
            "code": "9085/3",
            "description": "mixed germ cell tumor"
        },
        {
            "code": "9100/0",
            "description": "hydatidiform mole"
        },
        {
            "code": "9100/3",
            "description": "choriocarcinoma"
        },
        {
            "code": "9104/3",
            "description": "malignant placental site trophoblastic tumor"
        },
        {
            "code": "9110/3",
            "description": "mesonephroma, malignant"
        },
        {
            "code": "9120/0",
            "description": "hemangioma, nos"
        },
        {
            "code": "9120/3",
            "description": "angiosarcoma"
        },
        {
            "code": "9130/3",
            "description": "hemangioendothelioma, malignant"
        },
        {
            "code": "9133/3",
            "description": "epithelioid hemangioendothelioma, malignant"
        },
        {
            "code": "9140/3",
            "description": "kaposi sarcoma"
        },
        {
            "code": "9150/1",
            "description": "hemangiopericytoma, nos"
        },
        {
            "code": "9160/0",
            "description": "angiofibroma, nos"
        },
        {
            "code": "9161/1",
            "description": "hemangioblastoma"
        },
        {
            "code": "9180/3",
            "description": "osteosarcoma, nos"
        },
        {
            "code": "9181/3",
            "description": "chondroblastic osteosarcoma"
        },
        {
            "code": "9182/3",
            "description": "fibroblastic osteosarcoma"
        },
        {
            "code": "9183/3",
            "description": "osteosarcoma, telangiectatic"
        },
        {
            "code": "9186/3",
            "description": "central osteosarcoma"
        },
        {
            "code": "9191/0",
            "description": "osteoid osteoma, nos"
        },
        {
            "code": "9200/0",
            "description": "osteoblastoma, nos"
        },
        {
            "code": "9210/0",
            "description": "osteochondroma"
        },
        {
            "code": "9220/0",
            "description": "chondroma, nos"
        },
        {
            "code": "9220/3",
            "description": "chondrosarcoma, nos"
        },
        {
            "code": "9230/0",
            "description": "chondroblastoma, nos"
        },
        {
            "code": "9241/0",
            "description": "chondromyxoid fibroma"
        },
        {
            "code": "9250/1",
            "description": "giant cell tumor of bone, nos"
        },
        {
            "code": "9260/3",
            "description": "ewing sarcoma"
        },
        {
            "code": "9270/3",
            "description": "odontogenic tumor, malignant"
        },
        {
            "code": "9310/3",
            "description": "ameloblastoma, malignant"
        },
        {
            "code": "9361/1",
            "description": "pineocytoma"
        },
        {
            "code": "9362/3",
            "description": "pineoblastoma"
        },
        {
            "code": "9364/3",
            "description": "peripheral neuroectodermal tumor, nos"
        },
        {
            "code": "9370/3",
            "description": "chordoma, nos"
        },
        {
            "code": "9380/3",
            "description": "glioma, nos"
        },
        {
            "code": "9381/3",
            "description": "gliomatosis cerebri"
        },
        {
            "code": "9382/3",
            "description": "glioma, mixed"
        },
        {
            "code": "9390/1",
            "description": "choroid plexus papilloma, nos"
        },
        {
            "code": "9390/3",
            "description": "choroid plexus papilloma, malignant"
        },
        {
            "code": "9391/3",
            "description": "ependymoma, nos"
        },
        {
            "code": "9392/3",
            "description": "ependymoblastoma"
        },
        {
            "code": "9394/1",
            "description": "myxopapillary ependymoma"
        },
        {
            "code": "9395/3",
            "description": "papillary tumor of the pineal region"
        },
        {
            "code": "9400/3",
            "description": "astrocytoma, nos"
        },
        {
            "code": "9401/3",
            "description": "astrocytoma, anaplastic"
        },
        {
            "code": "9410/3",
            "description": "protoplasmic astrocytoma"
        },
        {
            "code": "9411/3",
            "description": "gemistocytic astrocytoma"
        },
        {
            "code": "9412/1",
            "description": "desmoplastic infantile astrocytoma-ganglioglioma"
        },
        {
            "code": "9413/0",
            "description": "dysembryoplastic neuroepithelial tumour"
        },
        {
            "code": "9420/3",
            "description": "fibrillary astrocytoma"
        },
        {
            "code": "9421/1",
            "description": "pilocytic astrocytoma"
        },
        {
            "code": "9424/3",
            "description": "pleomorphic xanthoastrocytoma"
        },
        {
            "code": "9430/3",
            "description": "astroblastoma"
        },
        {
            "code": "9440/3",
            "description": "glioblastoma, nos"
        },
        {
            "code": "9442/3",
            "description": "gliosarcoma"
        },
        {
            "code": "9450/3",
            "description": "oligodendroglioma, nos"
        },
        {
            "code": "9451/3",
            "description": "oligodendroglioma, anaplastic"
        },
        {
            "code": "9470/3",
            "description": "medulloblastoma, nos"
        },
        {
            "code": "9471/3",
            "description": "medulloblastoma with extensive nodularity"
        },
        {
            "code": "9473/3",
            "description": "primitive neuroectodermal tumor, nos"
        },
        {
            "code": "9474/3",
            "description": "large cell medulloblastoma"
        },
        {
            "code": "9490/0",
            "description": "ganglioneuroma"
        },
        {
            "code": "9490/3",
            "description": "ganglioneuroblastoma"
        },
        {
            "code": "9500/3",
            "description": "neuroblastoma, nos"
        },
        {
            "code": "9501/3",
            "description": "medulloepithelioma, nos"
        },
        {
            "code": "9503/3",
            "description": "neuroepithelioma, nos"
        },
        {
            "code": "9505/1",
            "description": "ganglioglioma"
        },
        {
            "code": "9506/1",
            "description": "neurocytoma"
        },
        {
            "code": "9508/3",
            "description": "atypical teratoid/rhabdoid tumor"
        },
        {
            "code": "9509/1",
            "description": "papillary glioneural tumour"
        },
        {
            "code": "9510/3",
            "description": "retinoblastoma, nos"
        },
        {
            "code": "9522/3",
            "description": "olfactory neuroblastoma"
        },
        {
            "code": "9530/0",
            "description": "meningioma, nos"
        },
        {
            "code": "9530/3",
            "description": "meningioma, malignant"
        },
        {
            "code": "9532/3",
            "description": "fibrous meningioma, malignant"
        },
        {
            "code": "9535/3",
            "description": "hemangioblastic meningioma, malignant"
        },
        {
            "code": "9538/3",
            "description": "rhabdoid meningioma"
        },
        {
            "code": "9540/0",
            "description": "neurofibroma, nos"
        },
        {
            "code": "9540/3",
            "description": "malignant peripheral nerve sheath tumor"
        },
        {
            "code": "9560/0",
            "description": "schwannoma, nos"
        },
        {
            "code": "9581/3",
            "description": "alveolar soft part sarcoma"
        },
        {
            "code": "9582/0",
            "description": "granular cell tumor"
        },
        {
            "code": "9590/3",
            "description": "malignant lymphoma, nos"
        },
        {
            "code": "9591/3",
            "description": "malignant lymphoma, b-cell nos"
        },
        {
            "code": "9596/3",
            "description": "composite hodgkin and non-hodgkin lymphoma"
        },
        {
            "code": "9650/3",
            "description": "hodgkin lymphoma, nos"
        },
        {
            "code": "9651/3",
            "description": "classical hodgkin lymphoma, lymphocyte-rich"
        },
        {
            "code": "9652/3",
            "description": "classical hodgkin lymphoma, mixed cellularity, nos"
        },
        {
            "code": "9653/3",
            "description": "classical hodgkin lymphoma, lymphocyte depletion, nos"
        },
        {
            "code": "9659/3",
            "description": "hodgkin lymphoma, nodular lymphocyte predominance"
        },
        {
            "code": "9663/3",
            "description": "classical hodgkin lymphoma, nodular sclerosis, nos"
        },
        {
            "code": "9671/3",
            "description": "malignant lymphoma, lymphoplasmacytic"
        },
        {
            "code": "9673/3",
            "description": "mantle cell lymphoma"
        },
        {
            "code": "9678/3",
            "description": "primary effusion lymphoma"
        },
        {
            "code": "9679/3",
            "description": "mediastinal large b-cell lymphoma"
        },
        {
            "code": "9680/3",
            "description": "diffuse large b-cell lymphoma, nos"
        },
        {
            "code": "9684/3",
            "description": "ml, large b-cell, diffuse, immunoblastic, nos"
        },
        {
            "code": "9687/3",
            "description": "burkitt lymphoma, nos"
        },
        {
            "code": "9689/3",
            "description": "splenic marginal zone lymphoma, nos"
        },
        {
            "code": "9690/3",
            "description": "follicular lymphoma, nos"
        },
        {
            "code": "9691/3",
            "description": "follicular lymphoma, grade 2"
        },
        {
            "code": "9695/3",
            "description": "follicular lymphoma, grade 1"
        },
        {
            "code": "9698/3",
            "description": "follicular lymphoma, grade 3"
        },
        {
            "code": "9699/3",
            "description": "marginal zone lymphoma, nos"
        },
        {
            "code": "9700/3",
            "description": "mycosis fungoides"
        },
        {
            "code": "9701/3",
            "description": "sezary syndrome"
        },
        {
            "code": "9702/3",
            "description": "malignant lymphoma, t-cell nos"
        },
        {
            "code": "9705/3",
            "description": "peripheral t-cell lymphoma, nos"
        },
        {
            "code": "9709/3",
            "description": "primary cutaneous peripheral t-cell lymphoma, nos"
        },
        {
            "code": "9714/3",
            "description": "anaplastic large cell lymphoma, nos"
        },
        {
            "code": "9716/3",
            "description": "hepatosplenic gamma/delta t-cell lymphoma"
        },
        {
            "code": "9717/3",
            "description": "enteropathy type t-cell lymphoma"
        },
        {
            "code": "9718/3",
            "description": "anaplastic large cell lymphoma, cutaneous type"
        },
        {
            "code": "9719/3",
            "description": "nk/t-cell lymphoma"
        },
        {
            "code": "9727/3",
            "description": "blastic plasmacytoid dendritic cell neoplasm"
        },
        {
            "code": "9728/3",
            "description": "precursor b-cell lymphoblastic lymphoma"
        },
        {
            "code": "9729/3",
            "description": "precursor t-cell lymphoblastic lymphoma"
        },
        {
            "code": "9731/3",
            "description": "multiple myeloma"
        },
        {
            "code": "9732/3",
            "description": "plasma cell myeloma"
        },
        {
            "code": "9733/3",
            "description": "plasma cell leukemia"
        },
        {
            "code": "9734/3",
            "description": "plasmacytoma, extramedullary"
        },
        {
            "code": "9751/1",
            "description": "langerhans cell histiocytosis, nos"
        },
        {
            "code": "9757/3",
            "description": "dendritic cell sarcoma, nos"
        },
        {
            "code": "9761/3",
            "description": "waldenstrom macroglobulinemia"
        },
        {
            "code": "9765/1",
            "description": "monoclonal gammopathy"
        },
        {
            "code": "9800/3",
            "description": "leukemia, nos"
        },
        {
            "code": "9801/3",
            "description": "acute leukemia, nos"
        },
        {
            "code": "9805/3",
            "description": "acute biphenotypic leukemia"
        },
        {
            "code": "9823/3",
            "description": "b-cell chronic lymphocytic leukemia/small lymphocytic lymphoma"
        },
        {
            "code": "9826/3",
            "description": "acute lymphoblastic leukemia, mature b-cell type"
        },
        {
            "code": "9827/3",
            "description": "adult t-cell leukemia/lymphoma [htlv1 pos.]"
        },
        {
            "code": "9833/3",
            "description": "prolymphocytic leukemia, b-cell type"
        },
        {
            "code": "9834/3",
            "description": "prolymphocytic leukemia, t-cell type"
        },
        {
            "code": "9835/3",
            "description": "acute lymphoblastic leukemia, nos"
        },
        {
            "code": "9836/3",
            "description": "precursor b-cell lymphoblastic leukemia"
        },
        {
            "code": "9837/3",
            "description": "precursor t-cell lymphoblastic leukemia"
        },
        {
            "code": "9840/3",
            "description": "acute erythroid leukemia [fab m6]"
        },
        {
            "code": "9860/3",
            "description": "myeloid leukemia, nos"
        },
        {
            "code": "9861/3",
            "description": "acute myeloid leukemia, nos"
        },
        {
            "code": "9863/3",
            "description": "chronic myeloid leukemia, nos"
        },
        {
            "code": "9866/3",
            "description": "acute promyelocytic leukemia [fab type m3]"
        },
        {
            "code": "9867/3",
            "description": "acute myelomonocytic leukemia [fab type m4]"
        },
        {
            "code": "9872/3",
            "description": "acute myeloblastic leukemia with minimal differentiation [fab type m0]"
        },
        {
            "code": "9873/3",
            "description": "acute myeloblastic leukemia without maturation [fab type m1]"
        },
        {
            "code": "9874/3",
            "description": "acute myeloblastic leukemia with maturation [fab m2]"
        },
        {
            "code": "9875/3",
            "description": "chronic myelogenous leukemia"
        },
        {
            "code": "9876/3",
            "description": "atypical chronic myeloid leukemia, bcr/abl negative"
        },
        {
            "code": "9891/3",
            "description": "acute monoblastic leukemia with differentiation [fab m5b]"
        },
        {
            "code": "9895/3",
            "description": "acute myeloid leukemia with prior myelodysplastic syndrome"
        },
        {
            "code": "9910/3",
            "description": "acute megakaryoblastic leukemia [fab m7]"
        },
        {
            "code": "9920/3",
            "description": "therapy-related acute myeloid leukemia, nos"
        },
        {
            "code": "9931/3",
            "description": "acute myelofibrosis"
        },
        {
            "code": "9940/3",
            "description": "hairy cell leukemia"
        },
        {
            "code": "9945/3",
            "description": "chronic myelomonocytic leukemia, nos"
        },
        {
            "code": "9946/3",
            "description": "juvenile myelomonocytic leukemia"
        },
        {
            "code": "9948/3",
            "description": "aggressive nk-cell leukemia"
        },
        {
            "code": "9950/3",
            "description": "polycythemia vera"
        },
        {
            "code": "9960/3",
            "description": "chronic myeloproliferative disease, nos"
        },
        {
            "code": "9961/3",
            "description": "chronic idiopathic myelofibrosis"
        },
        {
            "code": "9962/3",
            "description": "essential thrombocythemia"
        },
        {
            "code": "9964/3",
            "description": "hypereosinophilic syndrome"
        },
        {
            "code": "9980/3",
            "description": "refractory anemia, nos"
        },
        {
            "code": "9982/3",
            "description": "refractory anemia with ringed sideroblasts [rars]"
        },
        {
            "code": "9983/3",
            "description": "refractory anemia with excess blasts [raeb]"
        },
        {
            "code": "9984/3",
            "description": "refractory anemia with excess blasts in transformation [raebt]"
        },
        {
            "code": "9985/3",
            "description": "refractory cytopenia with multilineage dysplasia"
        },
        {
            "code": "9986/3",
            "description": "myelodysplastic syndrome with 5q- syndrome"
        },
        {
            "code": "9987/3",
            "description": "therapy-related myelodysplastic syndrome, nos"
        },
        {
            "code": "9989/3",
            "description": "myelodysplastic syndrome, nos"
        }
    ];
}]);


arrayMap.controller('DocumentationCtrl', ['$scope', '$http', function ($scope, $http) {
    $http.get('/pages/doc.md').success(function (data){
        $scope.mkText = data;
    });
}]);

