This is an experimental version using node express and monk to connect to mongodb.
It was mostly generated using [express-generator](http://expressjs.com/en/starter/generator.html)

Usage:
```
npm install
npm start
```

The implementation was adapted to arraymap data based on this tutorial:
http://cwbuecheler.com/web/tutorials/2014/restful-web-app-node-express-mongodb/

Instead of using monk, one can also use mongoose

To make it REST, you may follow this:
http://cwbuecheler.com/web/tutorials/2014/restful-web-app-node-express-mongodb/

