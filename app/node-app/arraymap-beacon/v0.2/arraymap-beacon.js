var beacon = {};

beacon.info = require('./beacon-info.js').info

beacon.checkResultAndGetResponse = require('./beacon-query.js').checkResultAndGetResponse
beacon.checkPreconditions = require('./beacon-query.js').checkPreconditions
beacon.buildMongoQuery = require('./beacon-query.js').buildMongoQuery

module.exports = beacon;