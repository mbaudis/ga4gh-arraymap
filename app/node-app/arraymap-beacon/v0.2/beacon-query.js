var info = require('./beacon-info.js').info

//check https://genome.ucsc.edu/FAQ/FAQreleases.html
var referenceMap = {
    'GRCh38': 'SEGMENTS_HG38',
    'GRCh37': 'SEGMENTS_HG19',
    'GRCh36': 'SEGMENTS_HG18'
};

//correspondence to arraymap class
var variantClassMap = {
    "DEL": -1,
    "DUP": 1
};

// By default doe Maximilien takes Reference 37. https://github.com/maximilianh/ucscBeacon/blob/master/help.txt
//TODO Should it be the same as Maximilen? Is it specified by beacon documentaiton?    
var defaultReference = 'GRCh36';

function checkPreconditions(params) {
    
    if (!params.chromosome) {
        return {
            hasError: true,
            msg: "chromosome is not present"
        };
    }

    var chromosomeInvalid = {
        hasError: true,
        msg: params.chromosome + " chromosome not valid"
    };

    if (Number(params.chromosome)) {
        if (params.chromosome < 1 || params.chromosome > 23) {
            return chromosomeInvalid;
        }
    }
    else {
        if (params.chromosome.match(/^(X|Y)$/i) == null) {
            return chromosomeInvalid;
        }
    }

    if (!params.position) {
        return {
            hasError: true,
            msg: "position not present"
        };
    }

    if (!Number(params.position)) {
        return {
            hasError: true,
            msg: "position not a number"
        };
    }

    console.log(Boolean(variantClassMap[params.variantClass]));

    if (!params.variantClass) {
        return {
            hasError: true,
            msg: "variant class is not defined, arrayMap supports be DUP or DEL"
        };
    }

    if (!variantClassMap[params.variantClass]) {
        return {
            hasError: true,
            msg: params.variantClass + " variant class is not supported by arrayMap , only supports DUP or DEL"
        };
    } //TODO add additional checks

    return {
        hasError: false
    };

}



function buildMongoQuery(params) {

    var position = parseInt(params.position);

    var conditions = [];
    //add constraint on dataset if it exsits
    if (params.dataset) conditions.push({
        ICDMORPHOLOGYCODE: params.dataset
    });

    var segType = variantClassMap[params.variantClass]

    var convertedReference = referenceMap[params.reference || defaultReference];

    var cond = {};
    cond[convertedReference] = {
        '$elemMatch': {
            'CHRO': params.chromosome,
            'SEGTYPE': segType,
            'SEGSTOP': {
                $gte: position
            },
            'SEGSTART': {
                $lte: position
            }
        }
    }

    conditions.push(cond);
    return {
        $and: conditions
    };
}

function checkResultAndGetResponse(params, samples, countTotal) {

    var responseResource = {
        "exists": null,
        "info": "ok",
        "error": null
    }


    if (samples && samples.length > 0) { // a value was found by mongodb

        responseResource.observed = samples.length;
        var matchedSegments = samples.map(function (s) { return checkResult(params, s)});

        if (!matchedSegments || !matchedSegments[0] || !matchedSegments[0].matchedSegment) {
            responseResource.exists = null;
            responseResource.error = "Internal error, DB returned a value but post check is not valid."
        } else {
            responseResource.exists = "overlap";
            responseResource.NOT_BEACON_ARRAYMAP_DEBUG_INFO = {"matchedSegments" : matchedSegments};
        }

    }

    var queryResource = {
        "chromosome": params.chromosome,
        "position": params.position,
        "reference": params.reference,
        "datasetid": params.dataset
    };


    return {
        "beacon_id": info.id,
        "response": responseResource,
        "query": queryResource
    };

}

function checkResult(params, sample) {

    var convertedReference = referenceMap[params.reference || defaultReference];
    var expectedSegType = variantClassMap[params.variantClass]
    var position = parseInt(params.position);

    //TODO should check for dataset and genomre reference as well

    var foundSegment;


    sample[convertedReference].forEach(function (segment) {
        if (segment.CHRO === params.chromosome) {

            var posStart = parseInt(segment.SEGSTART);
            var posStop = parseInt(segment.SEGSTOP);
            var segType = parseInt(segment.SEGTYPE);

            if ((segType == expectedSegType) && (posStart <= position) && (position <= posStop)) {
                foundSegment = segment;
                return; //exit loop
            }
        }
    })


    var result = {
        matchedSampleUID: sample.UID,
        matchedDataSet: sample.ICDMORPHOLOGYCODE,
        matchedSegment: foundSegment
    }
    return result;
}


module.exports.checkPreconditions = checkPreconditions;
module.exports.buildMongoQuery = buildMongoQuery;
module.exports.checkResultAndGetResponse = checkResultAndGetResponse;
