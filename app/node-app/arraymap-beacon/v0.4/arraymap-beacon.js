var beacon = {};

beacon.info = require('./beacon-info.js').info

beacon.checkResultAndGetResponse = require('./beacon-query.js').checkResultAndGetResponse
beacon.checkPreconditions = require('./beacon-query.js').checkPreconditions
beacon.buildMongoQuery = require('./beacon-query.js').buildMongoQuery

beacon.checkDatasetIdentifier = require('./beacon-dataset.js').checkDatasetIdentifier
beacon.buildMongoDatasetQuery = require('./beacon-dataset.js').buildMongoDatasetQuery
beacon.checkDatasetResultAndGetResponse = require('./beacon-dataset.js').checkDatasetResultAndGetResponse

module.exports = beacon;